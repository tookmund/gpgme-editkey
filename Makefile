LDLIBS= -l gpgme
CFLAGS=-g -Wall

all: test revkey expirekey

test: test.o common.o

revkey: revkey.o common.o

expirekey: expirekey.o common.o
clean:
	rm -f test revkey expirekey
	rm -f *.o
