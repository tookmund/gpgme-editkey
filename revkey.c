#include <gpgme.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "common.h"

#define BUFSIZE 100

gpgme_error_t revkey(void *handle, const char* status, const char *args,
		int fd)
{
	interact_key_t *ik;
	ik = (interact_key_t *)handle;
	int stdstat = standardstatus(status, args);
	if (stdstat != -1) return stdstat;
	char result[BUFSIZE];
	memset(result, 0, BUFSIZE);
	int err;
	int size;
	switch(ik->step)
	{
		case 0:
			if (strcmp(status, "KEY_CONSIDERED") != 0) return GPG_ERR_FALSE;
			break;
		case 1:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			size = sprintf(result, "key %d\n", ik->keynum);
			if (size < 0)
			{
				perror("sprintf key %d");
				return GPG_ERR_FALSE;
			}
			err = writen(fd, result, size);
			if (err < 0)
			{
				perror("writen key %d");
				return GPG_ERR_FALSE;
			}
			break;
		case 2:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			size = sprintf(result, "revkey\n");
			if (size < 0)
			{
				perror("sprintf revkey");
				return GPG_ERR_FALSE;
			}
			err = writen(fd, result, size);
			if (err < 0)
			{
				perror("writen revkey");
				return GPG_ERR_FALSE;
			}
			break;
		case 3:
			if (getbool(status, fd, result)) return GPG_ERR_FALSE;
			break;
		case 4:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			size = sprintf(result, "%s\n", ik->revcode);
			if (size < 0)
			{
				perror("sprintf revcode");
				return GPG_ERR_FALSE;
			}
			err = writen(fd, result, size);
			if (err < 0)
			{
				perror("writen revcode");
				return GPG_ERR_FALSE;
			}
			break;
		case 5:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			size = sprintf(result, "%s\n", ik->revtext);
			if (size < 0)
			{
				perror("sprintf revtext");
				return GPG_ERR_FALSE;
			}
			err = writen(fd, result, size);
			if (err < 0)
			{
				perror("writen revtext");
				return GPG_ERR_FALSE;
			}
			break;
		case 6:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			err = writen(fd, "\n", 1);
			if (err < 0)
			{
				perror("writen endrevtext");
				return GPG_ERR_FALSE;
			}
			break;
		case 7:
			if (getbool(status, fd, result)) return GPG_ERR_FALSE;
			break;
		case 8:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			err = writen(fd, "save\n", 5);
			if (err < 0)
			{
				perror("writen save");
				return GPG_ERR_FALSE;
			}
			break;
		case 9:
			return 0;
		default:
			return GPG_ERR_FALSE;
	}
	ik->step++;
	return 0;
}

int main(int argc, char** argv)
{
	if (argc < 6)
	{
		fprintf(stderr, "%s homedir masterkey subkey revcode revtext\n",
				argv[0]);
		return 1;
	}
	gpgme_ctx_t ctx;
	gpgme_key_t key;
	interact_key_t ik;
	char* homedir = argv[1];
	char* masteikeyid = argv[2];
	ik.fpr = argv[3];
	ik.revcode = argv[4];
	ik.revtext = argv[5];
	gpgme_error_t err = setup(&ctx, &key, homedir, masteikeyid);
	if (err)
	{
		fprintf(stderr, "%s", "Setup: ");
		printerror(err);
		return 1;
	}

	ik.step = 0;
	setkeynum(key->subkeys, &ik);
	if (ik.keynum < 0)
	{
		fprintf(stderr, "%s", "Setkey");
		return 1;
	}

	gpgme_data_t data;
	err = gpgme_data_new(&data);
	if (err)
	{
		fprintf(stderr,"%s", "Data: ");
		printerror(err);
		return 1;
	}
	err = gpgme_op_interact(ctx, key, 0, revkey, &ik, data);
	if (err)
	{
		fprintf(stderr, "%s", "revkey:");
		printerror(err);
	}
	gpgme_data_release(data);
	return 0;
}
