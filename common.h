
int writen(const int sd, const char * b, const size_t s);

int checkerror(gpgme_error_t err);

void printerror(gpgme_error_t err);

gpgme_error_t setup(gpgme_ctx_t *ctx, gpgme_key_t *key, char* homedir, char* keyid);

typedef struct interact_key
{
	int step;
	int keynum;
	char *fpr;
	char *expire;
	char *revcode;
	char *revtext;
} interact_key_t;

int getkeynum(gpgme_subkey_t subkeys, char *fpr);

void setkeynum(gpgme_subkey_t sk, interact_key_t *ik);

int setkey(const char *status, int fd, char *result, int keynum);

int inputcheck(const char *status, int fd);

int getbool(const char *status, int fd, char *result);

int standardstatus(const char *status, const char *args);
