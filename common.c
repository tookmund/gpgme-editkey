#include <gpgme.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "common.h"

// https://stackoverflow.com/a/24260280
int writen(const int sd, const char * b, const size_t s)
{
  size_t n = s;
  while (0 < n)
  {
    ssize_t result = write(sd, b, n);
    if (result < 0)
    {
      if ((errno == EINTR) || (errno == EWOULDBLOCK) || (errno == EAGAIN))
      {
        continue;
      }
      else
      {
        break;
      }
    }
    n -= result;
    b += result;
  }
  return (0 < n) ? -1 : 0;
}
int checkerror(gpgme_error_t err)
{
	int errcode = gpgme_err_code(err);
	if (errcode == GPG_ERR_NO_ERROR) return 1;
	else return 0;
}

void printerror(gpgme_error_t err)
{
	const char *strerror = gpgme_strerror(err);
	const char *strsource = gpgme_strsource(err);
	fprintf(stderr, "%s: %s\n", strsource, strerror);
}

gpgme_error_t setup(gpgme_ctx_t *ctx, gpgme_key_t *key, char* homedir, char* keyid)
{
	// Initializes GPGME
	gpgme_check_version(NULL);
	gpgme_error_t err;
	err = gpgme_new(ctx);
	if (checkerror(err))
	{
		gpgme_engine_info_t eng = gpgme_ctx_get_engine_info(*ctx);
		err = gpgme_ctx_set_engine_info(*ctx, GPGME_PROTOCOL_OPENPGP,
				eng->file_name, homedir);
		if (checkerror(err))
		{
			err = gpgme_op_keylist_start(*ctx, keyid, 1);
			if (checkerror(err))
			{
				do {
					err = gpgme_op_keylist_next(*ctx, key);
					if (!checkerror(err)) return err;
				} while(strcmp((*key)->fpr, keyid) != 0);
				err = 0;
			}
		}
	}
	return err;
}

int getkeynum(gpgme_subkey_t sk, char *fpr)
{
	int i = 0;
	do {
		if (strcmp(fpr, sk->fpr) == 0) return i;
		i++;
		sk = sk->next;
	} while (sk != NULL);
	return -1;
}
void setkeynum(gpgme_subkey_t sk, interact_key_t *ik)
{
	ik->keynum = getkeynum(sk, ik->fpr);
}
int setkey(const char *status, int fd, char *result, int keynum)
{
	int size;
	gpgme_error_t err;
	if (inputcheck(status, fd)) return 1;
	size = sprintf(result, "key %d\n", keynum);
	if (size < 0)
	{
		perror("sprintf key %d");
		return 1;
	}
	err = writen(fd, result, size);
	if (err < 0)
	{
		perror("writen key %d");
		return 1;
	}
	return 0;
}
int inputcheck(const char *status, int fd)
{
	if (strcmp(status, "GET_LINE") != 0) return 1;
	if (fd < 0) return 1;
	return 0;
}
int getbool(const char *status, int fd, char *result)
{
	int err, size;
	if (strcmp(status, "GET_BOOL") != 0) return 1;
	if (fd < 0) return 1;
	size = sprintf(result, "yes\n");
	if (size < 0)
	{
		perror("sprintf yes");
		return 1;
	}
	err = writen(fd, result, size);
	if (err < 0)
	{
		perror("writen yes");
		return 1;
	}
	return 0;
}
int standardstatus(const char *status, const char *args)
{
	if (strcmp(status, "ERROR") == 0)
	{
		fprintf(stderr, "%s(%s)\n", status, args);
		return GPG_ERR_FALSE;
	}
	if(strcmp(status, "GOT_IT") == 0) return 0;
	return -1;
}
