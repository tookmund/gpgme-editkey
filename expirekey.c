#include <gpgme.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"

#define BUFSIZE 100

gpgme_error_t expirekey(void *handle, const char *status, const char *args,
		int fd)
{
	interact_key_t *ik;
	ik = (interact_key_t *)handle;
	int stdstat = standardstatus(status, args);
	if (stdstat != -1) return stdstat;
	char result[BUFSIZE];
	memset(result, 0, BUFSIZE);
	int err, size;
	switch (ik->step)
	{
		case 0:
			if (strcmp(status, "KEY_CONSIDERED") != 0) return GPG_ERR_FALSE;
			break;
		case 1:
			if (setkey(status, fd, result, ik->keynum)) return GPG_ERR_FALSE;
			break;
		case 2:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			err = writen(fd, "expire\n", 7);
			if (err < 0)
			{
				perror("writen expire");
				return GPG_ERR_FALSE;
			}
			break;
		case 3:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			size = sprintf(result, "%s\n", ik->expire);
			if (size < 0)
			{
				perror("sprintf setexpire");
				return GPG_ERR_FALSE;
			}
			err = writen(fd, result, size);
			if (err)
			{
				perror("writen setexpire");
				printerror(err);
				return GPG_ERR_FALSE;
			}
			break;
		case 4:
			if (inputcheck(status, fd)) return GPG_ERR_FALSE;
			err = writen(fd, "save\n", 5);
			if (err)
			{
				perror("writen save");
				printerror(err);
				return GPG_ERR_FALSE;
			}
			break;
		case 5:
			break;
		default:
			return GPG_ERR_FALSE;

	}
	ik->step++;
	return 0;
}
int main(int argc, char **argv)
{
	if (argc < 5)
	{
		fprintf(stderr, "%s homedir masterkey subkey days\n", argv[0]);
		return 1;
	}
	char *homedir = argv[1];
	char *masterkey = argv[2];
	interact_key_t ik;
	ik.step = 0;
	ik.fpr = argv[3];
	ik.expire = argv[4];
	gpgme_ctx_t ctx;
	gpgme_key_t key;
	gpgme_error_t err = setup(&ctx, &key, homedir, masterkey);
	if (err)
	{
		perror("setup");
		printerror(err);
		return 1;
	}
	setkeynum(key->subkeys, &ik);
	if (ik.keynum < 0)
	{
		fprintf(stderr, "%s", "setkeynum");
		return 1;
	}
	gpgme_data_t data;
	err = gpgme_data_new(&data);
	if (err)
	{
		fprintf(stderr, "%s", "data");
		printerror(err);
	}
	err = gpgme_op_interact(ctx, key, 0, expirekey, &ik, data);
	if (err)
	{
		fprintf(stderr, "%s", "expire");
		printerror(err);
	}
	gpgme_data_release(data);
	return 0;
}
