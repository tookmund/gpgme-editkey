#include <gpgme.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "common.h"


gpgme_error_t interact(void *handle, const char *status, const char *args,
		int fd)
{
	fprintf(stderr, "%s (%s) [%d]\n", status, args, fd);
	if (fd != -1)
	{
		char *line;
		size_t n = 0;
		ssize_t s = getline(&line, &n, stdin);
		int w = writen(fd, line, s);
		free(line);
		if (w < 0) return GPG_ERR_FALSE;
		else return 0;
	}
	return 0;
}

int main(int argc, char **argv)
{
	if (argc < 3)
	{
		fprintf(stderr, "%s homedir masterkey\n", argv[0]);
		return 1;
	}
	char *homedir = argv[1];
	char *keyid = argv[2];
	unsigned int flags = 0;
	if (argc > 3) flags = GPGME_INTERACT_CARD;
	gpgme_error_t err;
	gpgme_ctx_t ctx;
	gpgme_key_t key;
	err = setup(&ctx, &key, homedir, keyid);
	fprintf(stderr, "%s\n", "Setup");
	if (checkerror(err))
	{
		fprintf(stderr, "%s\n", key->fpr);
		gpgme_data_t data;
		err = gpgme_data_new_from_stream(&data, stderr);
		fprintf(stderr, "%s\n", "Data");
		if (checkerror(err))
		{
			fprintf(stderr, "%s\n", "Interact");
			err = gpgme_op_interact(ctx, key, flags, interact, NULL, data);
		}
	}
	if (!checkerror(err)) printerror(err);
}
